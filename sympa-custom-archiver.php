#!/usr/bin/php
<?php

require_once __DIR__.'/vendor/autoload.php';

$conf_file = '/etc/custom_archiver.yml';

function usage() {
    $script = basename(__FILE__);
    echo <<<EOF
custom_archiver.pl (c) 2023 Framasoft, WTFPL

USAGE
  $script --list foo@bar.org --file /var/lib/sympa/spool/msg/file.txt [--verbose]
  $script --reanalyze [--verbose]
  $script --clean [--verbose]
  $script --help

  Use the custom_archiver parameter in /etc/sympa/sympa.conf to use this script.

OPTIONS
  --list       The address of the list including domain part
  --file       Absolute path to the message to be archived
  --reanalyze  Reanalyze the archive directory and all its messages
  --clean      Remove mails older than `days_to_keep_messages` parameter
  --verbose    Increase verbosity
  --help       Print this help and exit

  If not using --help, both --list and --file options are mandatory.

CONFIGURATION
  Put the configuration in /etc/custom_archiver.yml
  See https://framagit.org/framasoft/framagroupes/sympa-custom-archiver/#configuration

SEE ALSO
  https://www.sympa.community/gpldoc/man/sympa_config.5.html#custom_archiver

EOF;
}

function check_requirements($clean = false, $reanalyze = false) {
    global $list, $file, $conf_file;
    $error = 0;

    if (! $clean && ! $reanalyze) {
        if (! isset($list)) {
            echo "  --list parameter is mandatory\n";
            $error++;
        }
        if (! isset($file)) {
            echo "  --file parameter is mandatory\n";
            $error += 2;
        }
    }
    if ($error) {
        echo "ERROR!\n";
        usage();
        exit($error);
    }

    if (is_readable($conf_file)) {
        $config = yaml_parse_file($conf_file);
    } else {
        echo "ERROR! $conf_file does not exist or is not readable. Exiting.\n";
        exit(4);
    }

    $sp = $config['store_path'];
    if (! is_dir($sp) || ! is_writable($sp)) {
        echo "The configured store_path ($sp) is not a directory or is not writable. Exiting.\n";
        exit(5);
    }

    if (! isset($config['db'])) {
        echo "The database credentials are not defined in $conf_file. Exiting.\n";
        exit(6);
    }
    if ($clean && ! isset($config['days_to_keep_messages'])) {
        echo "The days_to_keep_messages parameter is not defined in $conf_file. Exiting.\n";
        exit(7);
    }

    return $config;
}

function initialize_db($dbh) {
    pg_query($dbh, '
        CREATE TABLE IF NOT EXISTS lists (
            id   serial PRIMARY KEY,
            name text   UNIQUE NOT NULL
        );
    ');
    pg_query($dbh, '
        CREATE TABLE IF NOT EXISTS mails (
            message_id text PRIMARY KEY,
            subject    text,
            mailfrom   text,
            path       text UNIQUE NOT NULL,
            date       timestamp with time zone NOT NULL,
            list_id    integer NOT NULL REFERENCES lists(id) ON DELETE CASCADE,
            body       text,
            html       text,
            headers    text
        );
    ');
}

function prepare_insert_mail_query($dbh) {
    pg_prepare($dbh, 'insert_mail', 'INSERT INTO mails (message_id, subject, mailfrom, date, path, list_id, body, html, headers) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT DO NOTHING');
}

function move_file($dbh) {
    global $list, $file, $config;

    $store_folder = join(DIRECTORY_SEPARATOR, [$config['store_path'], 'sympa_archives']);
    if (! is_dir($store_folder)) {
        mkdir($store_folder, 0750, true);
    }

    $sth = pg_prepare($dbh, 'insert_list', 'INSERT INTO lists (name) VALUES ($1) ON CONFLICT (name) DO UPDATE SET name = $1 RETURNING id');
    $sth = pg_execute($dbh, 'insert_list', [$list]);
    $list_id = pg_fetch_row($sth)[0];

    $lf = join(DIRECTORY_SEPARATOR, [$store_folder, $list]);
    if (! is_dir($lf)) {
        mkdir($lf, 0750, true);
    }

    $nf = join(DIRECTORY_SEPARATOR, [$lf, basename($file)]);
    rename($file, $nf);

    return [$nf, $lf, $list_id];
}

function reanalyze($dbh) {
    global $config, $verbose;

    $store_folder = join(DIRECTORY_SEPARATOR, [$config['store_path'], 'sympa_archives']);

    $sth = pg_query($dbh, 'SELECT id, name FROM lists');

    while ($row = pg_fetch_row($sth)) {
        $list_id = $row[0];
        $list = $row[1];
        $lf = join(DIRECTORY_SEPARATOR, [$store_folder, $list]);

        $mails = scandir($lf);
        for ($i = 0, $size = count($mails); $i < $size; ++$i) {
            if ($mails[$i] != '..' && $mails[$i] != '.') {
                $nf = join(DIRECTORY_SEPARATOR, [$lf, $mails[$i]]);
                if ($verbose) {
                    echo "Reanalyzing $list/$mails[$i].\n";
                }
                analyze_mail($dbh, $nf, $lf, $list_id);
            }
        }
    }
}

function analyze_mail($dbh, $file, $list_folder, $list_id) {
    global $verbose;

    $parser = new PhpMimeMailParser\Parser();
    $parser->setText(file_get_contents($file));

    $subject = $parser->getHeader('subject');
    $message_id = $parser->getHeader('message-id');
    $from = $parser->getHeader('from');
    $date = preg_replace('/ \(.*\)$/', '', $parser->getHeader('date'));
    $text = $parser->getMessageBody('text');
    $html = $parser->getMessageBody('htmlEmbedded');
    $headers = $parser->getHeadersRaw();

    $sth = pg_execute($dbh, 'insert_mail', [$message_id, $subject, $from, $date, $file, $list_id, $text, $html, $headers]);

    if ($verbose) {
        echo "Mail $message_id successfully recorded.\n";
    }
}

function clean_messages($dbh) {
    global $config, $verbose;

    $sth = pg_prepare($dbh, 'get_old_mails', "SELECT message_id, path FROM mails WHERE date < NOW() - $1 * INTERVAL'1 DAY'");
    $sth = pg_execute($dbh, 'get_old_mails', [$config['days_to_keep_messages']]);

    pg_prepare($dbh, 'delete_old_mail', 'DELETE FROM mails where message_id = $1');
    while ($row = pg_fetch_assoc($sth)) {
        if (unlink($row['path'])) {
            pg_execute($dbh, 'delete_old_mail', [$row['message_id']]);
            if ($verbose) {
                echo "Mail " . $row['message_id'] . " successfully deleted.\n";
            }
        } else {
            echo "Unable to delete " . $row['path'] . ".\n";
        }
    }
}


$longopts = ['list:', 'file:', 'help', 'clean', 'verbose', 'reanalyze'];

$options = getopt('', $longopts);

if (isset($options['help'])) {
    usage();
    exit(0);
}

if (array_key_exists('list', $options)) {
    $list = $options['list'];
}
if (array_key_exists('file', $options)) {
    $file = $options['file'];
}
$clean = isset($options['clean']);
$verbose = isset($options['verbose']);
$reanalyze = isset($options['reanalyze']);

$config = check_requirements($clean, $reanalyze);

$pguser = $config['db']['user'];
$pgdb   = $config['db']['database'];
$pgpass = $config['db']['password'];

$dbconn = pg_connect("host=127.0.0.1 dbname=$pgdb user=$pguser password=$pgpass options='--client_encoding=UTF8'");

initialize_db($dbconn);

if ($clean) {
    clean_messages($dbconn);
} else if ($reanalyze) {
    prepare_insert_mail_query($dbconn);
    reanalyze($dbconn);
} else {
    [$new_file, $list_folder, $list_id] = move_file($dbconn);

    prepare_insert_mail_query($dbconn);
    analyze_mail($dbconn, $new_file, $list_folder, $list_id);
}
?>
