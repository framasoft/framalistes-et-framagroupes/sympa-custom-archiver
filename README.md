# Sympa custom archiver

## Dependencies

On Debian :
```bash
apt install php-mailparse php-pgsql php-yaml
```

Install Composer (see <https://getcomposer.org/download/>).

## Installation

```bash
git clone https://framagit.org/framasoft/framagroupes/sympa-custom-archiver
cd sympa-custom-archiver
composer install
```

Create PostgreSQL user and database :
```
sudo -u postgres createuser -P custom_archiver
sudo -u postgres createdb -O custom_archiver custom_archiver
```

## Configuration

Put your configuration in `/etc/custom_archiver.yml`, belonging to the user of Sympa and with permission 600.

```yaml
store_path: /var/lib/sympa-custom-archives
db:
  user: custom_archiver
  database: custom_archiver
  password: S3cr3t
days_to_keep_messages: 7
reporting_spam_script_path: /opt/learn_spam.sh
```

- `store_path` is the path where mail will be archived. MANDATORY
- `db` contains the database credentials. MANDATORY
- `days_to_keep_messages` is the age (in days) of the messages before being eligible to deletion. MANDATORY
- `reporting_spam_script_path` is the absolute path of the script to report spam. Optional.
  If set, this script can be invoked on a message which content is injected into standard input of the script.
  The message is then deleted from the archives.

## Configure Sympa

Set the `custom_archiver` to the absolute path of the `sympa-custom-archiver.php` script in your Sympa configuration.

See <https://www.sympa.community/gpldoc/man/sympa_config.5.html#custom_archiver>.

## Old messages deletion

Put this command in a daily cron job (adapt to the path where you installed `sympa-custom-archiver`) :
```bash
sympa-custom-archiver.php --clean
```

Messages older than `days_to_keep_messages` config parameter will be deleted from filesystem and database.
